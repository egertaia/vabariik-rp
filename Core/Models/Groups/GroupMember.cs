﻿using System;
using System.Collections.Generic;
using Vabariik.Core.Extensions;
using Vabariik.Core.Models.Economy;
using Vabariik.Core.Models.Player;

namespace Vabariik.Core.Models.Groups
{
	public class GroupMember : Model, IGroupMember
    {
		public virtual Character Character { get; set; }
		public virtual List<GroupRole> Roles { get; set; }
        public virtual Group Group { get; set; }
        public virtual List<Salary> Salaries { get; set; }

		public GroupMember()
		{
			this.Id = GuidGenerator.GenerateTimeBasedGuid();
			this.Created = DateTime.UtcNow;
		}
	}
}
