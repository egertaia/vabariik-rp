﻿using Vabariik.Core.Models.Inventories;

namespace Vabariik.Core.Models.Objects
{
    public interface IStorable
    {
        int Size { get; set; }
        bool IsStored { get; set; }
        Container Container { get; set; }
    }
}
