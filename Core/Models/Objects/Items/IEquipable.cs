﻿namespace Vabariik.Core.Models.Objects.Items
{
    public interface IEquipable
    {
        bool IsEquiped { get; set; }
    }
}
