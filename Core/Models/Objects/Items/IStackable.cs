﻿namespace Vabariik.Core.Models.Objects.Items
{
    public interface IStackable
    {
        int Quantity { get; set; }
    }
}
