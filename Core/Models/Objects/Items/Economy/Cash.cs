﻿namespace Vabariik.Core.Models.Objects.Items.Economy
{
	public class Cash : StorableItem, IStackable
	{
		public int Quantity { get; set; }
	}
}
