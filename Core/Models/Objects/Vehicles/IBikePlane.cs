﻿namespace Vabariik.Core.Models.Objects.Vehicles
{
    public interface IBikePlane : IRoadVehicle, IAerialVehicle
    {
    }
}
