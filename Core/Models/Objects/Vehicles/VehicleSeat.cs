﻿using System;
using Vabariik.Core.Extensions;
using Vabariik.Core.Models.Player;

namespace Vabariik.Core.Models.Objects.Vehicles
{
    public class VehicleSeat
    {
        public Guid Id { get; set; }
        public VehicleSeatIndex Index { get; set; }
        public ICharacter Character { get; set; }

        public VehicleSeat() { this.Id = GuidGenerator.GenerateTimeBasedGuid(); }
    }
}
