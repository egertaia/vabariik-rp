﻿namespace Vabariik.Core.Models.Objects.Vehicles
{
    public interface ISeaPlane : IAerialVehicle, ISeaVehicle
    {
    }
}
