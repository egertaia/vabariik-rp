﻿using System;
using JetBrains.Annotations;
using Vabariik.Core.Models.Groups;

namespace Vabariik.Core.Models.Economy
{
    public class Salary : Model
    {
        public TimeSpan Frequency { get; set; }
        public double Amount { get; set; }
        [CanBeNull] public Position Position { get; set; }
        [CanBeNull] public float Radius { get; set; }
        public bool Active { get; set; }

		public virtual GroupMember Member { get; set; }
    }
}
