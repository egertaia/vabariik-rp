﻿using System;
using Vabariik.Core.Models.Appearance;

namespace Vabariik.Core.Models.Inventories.Characters
{
    public interface IProp
    {
        Guid Id { get; set; }
        Prop Value { get; set; }
    }
}
