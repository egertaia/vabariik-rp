﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Vabariik.Core.Extensions;

namespace Vabariik.Core.Models.Inventories.Characters
{
    [Table("arms_arms")]
    public class Arm
    {
        [Key] public Guid Id { get; set; } = GuidGenerator.GenerateTimeBasedGuid();
    }
}
