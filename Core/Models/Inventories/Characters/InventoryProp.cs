﻿using System;
using System.ComponentModel.DataAnnotations;
using Vabariik.Core.Extensions;
using Vabariik.Core.Models.Appearance;

namespace Vabariik.Core.Models.Inventories.Characters
{
    public class InventoryProp : IProp
    {
        [Key] public Guid Id { get; set; } = GuidGenerator.GenerateTimeBasedGuid();
        public Prop Value { get; set; }
    }
}
