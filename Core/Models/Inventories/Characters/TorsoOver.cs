﻿using System.Collections.Generic;

namespace Vabariik.Core.Models.Inventories.Characters
{
    public class TorsoOver : InventoryComponent
    {
        public virtual List<Pocket> Pockets { get; set; }
    }
}
