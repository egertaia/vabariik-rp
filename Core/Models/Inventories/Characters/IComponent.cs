﻿using System;
using Vabariik.Core.Models.Appearance;

namespace Vabariik.Core.Models.Inventories.Characters
{
    public interface IComponent
    {
        Guid Id { get; set; }
        Component Component { get; set; }
    }
}
