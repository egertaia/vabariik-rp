﻿using System;
using System.ComponentModel.DataAnnotations;
using Vabariik.Core.Extensions;
using Vabariik.Core.Models.Objects.Items;

namespace Vabariik.Core.Models.Inventories.Characters
{
    public class Holster
    {
        [Key] public Guid Id { get; set; } = GuidGenerator.GenerateTimeBasedGuid();
        public StorableItem Item { get; set; }
        public int Size { get; set; }
    }
}
