﻿using System;
using System.Collections.Generic;
using Vabariik.Core.Models.Objects.Items;

namespace Vabariik.Core.Models.Inventories
{
    public interface IContainer
    {
        Guid Id { get; set; }
        List<StorableItem> Storage { get; set; }
        int Size { get; set; }
    }
}
