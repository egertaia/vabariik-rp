﻿namespace Vabariik.Core.Rpc
{
	public static class RpcEvents
	{
		public const string GetServerInformation = "vabariik:client:ready";
		public const string ClientDisconnect = "vabariik:client:disconnect";

		public const string GetUser = "vabariik:user:load";
		public const string GetCharacters = "vabariik:user:characters";
		public const string AcceptRules = "vabariik:user:rules";

		public const string CharacterCreate = "vabariik:character:create";
		public const string CharacterLoad = "vabariik:character:load";
		public const string CharacterDelete = "vabariik:character:delete";
		public const string CharacterSave = "vabariik:character:save";
		public const string CharacterRevive = "vabariik:character:revive";
		public const string GetCharacterPosition = "vabariik:character:getpos";

		public const string CharacterComponentSet = "vabariik:character:component:set";
		public const string CharacterPropSet = "vabariik:character:prop:set";
		
		public const string BankAtmWithdraw = "vabariik:bank:atm:withdraw";
		public const string BankBranchWithdraw = "vabariik:bank:branch:withdraw";
		public const string BankBranchDeposit = "vabariik:bank:branch:withdraw";
		public const string BankBranchTransfer = "vabariik:bank:branch:transfer";
		public const string BankOnlineTransfer = "vabariik:bank:online:transfer";

		public const string EntityDelete = "vabariik:entity:delete";

		public const string CarCreate = "vabariik:car:create";
		public const string CarSpawn = "vabariik:car:spawn";
		public const string CarSave = "vabariik:car:save";
		public const string CarTransfer = "vabariik:car:transfer";
		public const string CarClaim = "vabariik:car:claim";
		public const string CarUnclaim = "vabariik:car:unclaim";

		public const string BikeSpawn = "vabariik:bike:spawn";
		public const string BikeSave = "vabariik:bike:save";
		public const string BikeTransfer = "vabariik:bike:transfer";
		public const string BikeClaim = "vabariik:bike:claim";
		public const string BikeUnclaim = "vabariik:bike:unclaim";
	}
}
