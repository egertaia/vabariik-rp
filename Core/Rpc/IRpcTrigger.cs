﻿namespace Vabariik.Core.Rpc
{
	public interface IRpcTrigger
	{
		void Fire(RpcMessage message);
	}
}
