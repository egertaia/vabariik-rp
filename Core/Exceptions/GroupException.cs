﻿namespace Vabariik.Core.Exceptions
{
    public class GroupException : ModelException
    {
        public GroupException(string message) : base(message) { }
    }
}
