﻿using System;

namespace Vabariik.Core.Exceptions
{
    public class ModelException : Exception
    {
        public ModelException(string message) : base(message) { }
    }
}
