# Vabariik RP
Created specially for vabariik rp server.

## Development
Building the project will require [Visual Studio 2017](https://www.visualstudio.com/) and [Node.js](https://nodejs.org/) to be installed. A MySQL database is required for storage, [MariaDB](https://mariadb.org/) is recommended.

This resource currently replaces *all* stock server resources; make sure you remove them from your configuration. The server will always try to load ``sessionmanager``, even if it is not in your configuration, so you must delete or rename the resource.

1. Clone this repo inside of your FiveM server's ``resources`` directory:
  ```sh
  git clone https://gitlab.com/egertaia/igicore.git vabariikrp
  cd vabariikrp
  ```

2. Build the project in Visual Studio.

3. Install the interface dependencies:
  ```sh
  cd Interface
  npm install
  ```

4. Build the interface:
  ```sh
  npm run build
  ```

5. Edit your ``server.cfg`` file to include the following line below your existing configuration:
  ```
  start vabariik-rp
  ```

6. Edit ``vabariikcore.yml`` with your database connection information and server details as needed.

Note: You may need to manually preconfigure your MySQL server to default the character set to Unicode. For MariaDB add ``--character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci`` to the server arguments.

### Forked & modified from igicore
[Igicore](https://github.com/Igirisujin/igicore/)