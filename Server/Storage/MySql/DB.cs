﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MySql.Data.Entity;
using Vabariik.Core.Models.Appearance;
using Vabariik.Core.Models.Economy.Banking;
using Vabariik.Core.Models.Groups;
using Vabariik.Core.Models.Inventories.Characters;
using Vabariik.Core.Models.Objects.Vehicles;
using Vabariik.Core.Models.Player;
using Vabariik.Server.Managers;
using Vabariik.Server.Migrations;
using Vabariik.Server.Models.Economy.Banking;
using Vabariik.Server.Models.Player;

namespace Vabariik.Server.Storage.MySql
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class DB : DbContext
    {
        public DbSet<Session> Sessions { get; set; }

        public DbSet<User> Users { get; set; }
        public DbSet<Character> Characters { get; set; }

        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupMember> GroupMembers { get; set; }
        public DbSet<GroupRole> GroupRoles { get; set; }

        public DbSet<Style> Styles { get; set; }

        public DbSet<Vehicle> Vehicles { get; set; }
	    public DbSet<VehicleExtra> VehicleExtras { get; set; }
	    public DbSet<VehicleWheel> VehicleWheels { get; set; }
	    public DbSet<VehicleWindow> VehicleWindows { get; set; }
	    public DbSet<VehicleMod> VehicleMods { get; set; }
	    public DbSet<VehicleDoor> VehicleDoors { get; set; }
	    public DbSet<VehicleSeat> VehicleSeats { get; set; }

		public DbSet<Car> Cars { get; set; }
        public DbSet<Bike> Bikes { get; set; }

        public DbSet<Inventory> Inventories { get; set; }

        public DbSet<Bank> Banks { get; set; }
        public DbSet<BankBranch> BankBranches { get; set; }
        public DbSet<BankAtm> BankAtms { get; set; }
        public DbSet<BankAccount> BankAccounts { get; set; }
        public DbSet<BankAccountCard> BankAccountCards { get; set; }
        public DbSet<BankAccountMember> BankAccountMembers { get; set; }

		public DB() : base(ConfigurationManager.Configuration.Database.ToString())
		{
			Database.SetInitializer(new MigrateDatabaseToLatestVersion<DB, Configuration>());
			
				this.Database.Log = m => Server.Log(m);
		}

	    public void UpdatePropertyList<T>(List<dynamic> src, List<dynamic> dest) where T : class
		{
		    foreach (var item in dest.ToList())
		    {
			    if (src.All(i => i.Index != item.Index)) this.Set<T>().Remove(item);
		    }

		    foreach (var item in src)
		    {
			    var model = dest.SingleOrDefault(i => i.Index == item.Index);

			    if (model != default(T))
			    {
				    item.Id = model.Id;
				    Server.Db.Entry(model).CurrentValues.SetValues(item);
			    }
			    else
			    {
				    dest.Add(item);
			    }
		    }
	    }
	}
}
