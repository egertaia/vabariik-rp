﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CitizenFX.Core;
using Vabariik.Server.Rpc;

namespace Vabariik.Server.Commands
{
	public class GpsCommand : Command
	{
		public override string Name => "gps";

		public override async Task RunCommand(Player player, List<string> args)
		{
			player
				.Event("vabariik:dev:gps")
				.Trigger();
		}
	}
}
