﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CitizenFX.Core;
using Vabariik.Core.Rpc;
using Vabariik.Server.Rpc;

namespace Vabariik.Server.Commands
{
	public class PropCommand : Command
	{
		public override string Name => "prop";

		public override async Task RunCommand(Player player, List<string> args)
		{
			player
				.Event(RpcEvents.CharacterPropSet)
				.Attach(int.Parse(args[0]))
				.Attach(int.Parse(args[1]))
				.Attach(int.Parse(args[2]))
				.Trigger();
		}
	}
}
