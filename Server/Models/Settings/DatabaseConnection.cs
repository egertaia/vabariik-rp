﻿namespace Vabariik.Server.Models.Settings
{
	public class DatabaseConnection
	{
		public string Host { get; protected set; } = "127.0.0.1";
		public int Port { get; protected set; } = 3306;
		public string Database { get; protected set; } = "vabariik";
		public string User { get; protected set; } = "egert";
		public string Password { get; protected set; } = "localhostpw";
		public string Charset { get; protected set; } = "utf8mb4";
		
		public override string ToString() => $"server={this.Host};port={this.Port};database={this.Database};user={this.User};password={this.Password};charset={this.Charset}";
	}
}
