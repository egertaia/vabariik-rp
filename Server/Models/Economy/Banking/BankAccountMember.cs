﻿using System;
using Vabariik.Core.Extensions;
using Vabariik.Core.Models.Economy.Banking;
using Vabariik.Core.Models.Player;

namespace Vabariik.Server.Models.Economy.Banking
{
	public class BankAccountMember : IBankAccountMembers
	{
		public Guid Id { get; set; }
		public DateTime Created { get; set; }
		public DateTime? Deleted { get; set; }

		public virtual BankAccount Account { get; set; }
		public virtual Character Member { get; set; }

		public BankAccountMember()
		{
			this.Id = GuidGenerator.GenerateTimeBasedGuid();
			this.Created = DateTime.UtcNow;
		}
	}
}
