﻿using System.Threading.Tasks;
using Vabariik.Core.Models.Player;
using Vabariik.Core.Services;

namespace Vabariik.Server.Services
{
    public abstract class ServerService : Service, IServerService
    {
        public virtual async Task<Character> OnCharacterCreate(Character character) => character;
    }
}
