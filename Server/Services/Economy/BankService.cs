﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vabariik.Core.Models.Economy.Banking;
using Vabariik.Core.Models.Player;
using Vabariik.Server.Models.Economy.Banking;

namespace Vabariik.Server.Services.Economy
{
	public class BankService : ServerService
	{
		public override void Initialize() { }

		public override async Task<Character> OnCharacterCreate(Character character)
		{
			var bank = Server.Db.Banks.FirstOrDefault();

			if (bank == null) return character;

			bank.Accounts.Add(new BankAccount
			{
				Balance = 10000,
				AccountNumber = 123454321,
				Type = BankAccountTypes.Personal,
				Members = new List<BankAccountMember>
				{
					new BankAccountMember
					{
						Member = character
					}
				}
			});
			await Server.Db.SaveChangesAsync();

			return await base.OnCharacterCreate(character);
		}
	}
}
