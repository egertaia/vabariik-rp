﻿using System.Threading.Tasks;
using Vabariik.Core.Models.Player;
using Vabariik.Server.Controllers;
using Vabariik.Server.Models.Player;
using Citizen = CitizenFX.Core.Player;

namespace Vabariik.Server.Extentions
{
	public static class CitizenExtentions
	{
		public static async Task<Character> ToLastCharacter(this Citizen citizen) => await CharacterController.GetLatestOrCreate(await User.GetOrCreate(citizen));
	}
}
