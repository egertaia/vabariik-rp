﻿using System;
using Vabariik.Client.Models;

namespace Vabariik.Client.Events
{
	public class UserEventArgs : EventArgs
	{
		public User User { get; }

		public UserEventArgs(User user)
		{
			this.User = user;
		}
	}
}
