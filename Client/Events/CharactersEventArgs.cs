﻿using System;
using System.Collections.Generic;
using Vabariik.Client.Models;

namespace Vabariik.Client.Events
{
	public class CharactersEventArgs : EventArgs
	{
		public List<Character> Characters { get; }

		public CharactersEventArgs(List<Character> characters)
		{
			this.Characters = characters;
		}
	}
}
