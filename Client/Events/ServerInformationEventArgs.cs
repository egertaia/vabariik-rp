﻿using System;
using Vabariik.Core.Models.Connection;

namespace Vabariik.Client.Events
{
	public class ServerInformationEventArgs : EventArgs
	{
		public ServerInformation Information { get; }

		public ServerInformationEventArgs(ServerInformation information)
		{
			this.Information = information;
		}
	}
}
