﻿using System;
using Vabariik.Client.Models;

namespace Vabariik.Client.Events
{
	public class CharacterEventArgs : EventArgs
	{
		public Character Character { get; }

		public CharacterEventArgs(Character character)
		{
			this.Character = character;
		}
	}
}
