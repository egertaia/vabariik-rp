using System;
using Vabariik.Core.Models.Player;

namespace Vabariik.Client.Models
{
    public class User : IUser
    {
        public Character Character { get; set; }
        public Guid Id { get; set; }
        public string SteamId { get; set; }
        public string Name { get; set; }
	    public DateTime? AcceptedRules { get; set; }
	    public DateTime Created { get; set; }
    }
}
