﻿using System;
using Vabariik.Client.Events;
using Vabariik.Client.Models;
using Vabariik.Client.Rpc;
using Vabariik.Core.Controllers;
using Vabariik.Core.Rpc;

namespace Vabariik.Client.Controllers.Player
{
	public class UserController : Controller
	{
		public event EventHandler<UserEventArgs> OnUserLoaded;

		/// <summary>
		/// Gets or sets the currently loaded user.
		/// </summary>
		/// <value>
		/// The loaded user.
		/// </value>
		public User User { get; protected set; }

		public async void Load()
		{
			// Load user
			this.User = await Server
				.Event(RpcEvents.GetUser)
				.Request<User>();

			this.OnUserLoaded?.Invoke(this, new UserEventArgs(this.User));
		}
	}
}
