﻿using System;
using System.Threading.Tasks;
using Vabariik.Client.Controllers.Player;
using Vabariik.Client.Events;
using Vabariik.Client.Rpc;
using Vabariik.Core.Controllers;
using Vabariik.Core.Models.Connection;
using Vabariik.Core.Rpc;

namespace Vabariik.Client.Controllers
{
	public class ClientController : Controller
	{
		public event EventHandler<ServerInformationEventArgs> OnClientReady;

		/// <summary>
		/// Loads initial data from the server, raises events and attaches handlers.
		/// </summary>
		public async Task Startup()
		{
			Client.Log("Startup");

			// Load server details
			this.OnClientReady?.Invoke(this, new ServerInformationEventArgs(await Server
				.Event(RpcEvents.GetServerInformation)
				.Request<ServerInformation>()
			));
			// Load user
			Client.Instance.Controllers.First<UserController>().Load();
			// Load user's characters
			Client.Instance.Controllers.First<CharacterController>().GetCharacters();

			Client.Log("Waiting for character selection...");
		}
	}
}
