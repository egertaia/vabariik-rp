﻿using Vabariik.Core.Utility;

namespace Vabariik.Client.Services
{
	public class ServiceRegistry : Registry<ClientService>
	{
		public void Initialize()
		{
			foreach (var service in this) Client.Instance.AttachTickHandler(service.Tick);
		}
	}
}
