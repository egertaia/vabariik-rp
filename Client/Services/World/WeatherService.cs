using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using CitizenFX.Core.UI;

namespace Vabariik.Client.Services.World
{
	public class WeatherService : ClientService
	{
		protected static readonly string[] WeatherNames = {
			"EXTRASUNNY",
			"CLEAR",
			"CLOUDS",
			"SMOG",
			"FOGGY",
			"OVERCAST",
			"RAIN",
			"THUNDER",
			"CLEARING",
			"NEUTRAL",
			"SNOW",
			"BLIZZARD",
			"SNOWLIGHT",
			"XMAS"
		};

		public Weather Current { get; set; } = Weather.Christmas;
		public Weather Last { get; protected set; } = Weather.ExtraSunny;

		public override async Task Tick()
		{
			if (this.Last != this.Current)
			{
				this.Last = this.Current;

				Screen.ShowNotification("SetWeatherTypeOverTime");
				API.SetWeatherTypeOverTime(WeatherNames[(int)this.Last], 15);

				await BaseScript.Delay(15000);

				Screen.ShowNotification("Done");

				if (this.Current == Weather.ExtraSunny)
				{
					this.Current = Weather.Christmas;
				}
				else
				{
					this.Current = Weather.ExtraSunny;
				}
			}

			API.ClearOverrideWeather();
			API.ClearWeatherTypePersist();
			API.SetWeatherTypePersist(WeatherNames[(int)this.Last]);
			API.SetWeatherTypeNow(WeatherNames[(int)this.Last]);
			API.SetWeatherTypeNowPersist(WeatherNames[(int)this.Last]);

			API.SetForceVehicleTrails(this.Last == Weather.Christmas);
			API.SetForcePedFootstepsTracks(this.Last == Weather.Christmas);
		}
	}
}
