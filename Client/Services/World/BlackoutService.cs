using System.Threading.Tasks;

namespace Vabariik.Client.Services.World
{
	public class BlackoutService : ClientService
	{
		public bool Enabled { get; set; } = false;

		public override async Task Tick()
		{
			CitizenFX.Core.World.Blackout = this.Enabled;
		}
	}
}
