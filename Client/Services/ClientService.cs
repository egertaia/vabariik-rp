﻿using System.Threading.Tasks;

namespace Vabariik.Client.Services
{
	public abstract class ClientService : IClientService
	{
		public abstract Task Tick();
	}
}
