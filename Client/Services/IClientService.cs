﻿using System.Threading.Tasks;

namespace Vabariik.Client.Services
{
	public interface IClientService
	{
		Task Tick();
	}
}
