﻿using Newtonsoft.Json;
using Vabariik.Core.Rpc;

namespace Vabariik.Client.Rpc
{
	public class RpcSerializer : IRpcSerializer
	{
		public string Serialize(object obj) => JsonConvert.SerializeObject(obj);

		public T Deserialize<T>(string data) => JsonConvert.DeserializeObject<T>(data);
	}
}
