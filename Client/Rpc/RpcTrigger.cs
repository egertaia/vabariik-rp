﻿using CitizenFX.Core;
using Vabariik.Core.Rpc;

namespace Vabariik.Client.Rpc
{
	public class RpcTrigger : IRpcTrigger
	{
		public void Fire(RpcMessage message)
		{
			//Client.Log($"Fire: \"{message.Event}\" with {message.Payloads.Count} payload(s):");

			BaseScript.TriggerServerEvent(message.Event, message.Build());
		}
	}
}
