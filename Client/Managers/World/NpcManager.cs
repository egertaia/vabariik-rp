﻿using System.Collections.Generic;
using Vabariik.Client.Models.World;

namespace Vabariik.Client.Managers.World
{
    public class NpcManager : Manager
    {
        public List<Npc> Npcs { get; set; }

        public override void Dispose()
        {

        }
    }
}
