﻿using System;

namespace Vabariik.Client.Managers
{
    public abstract class Manager : IManager, IDisposable
    {
	    public abstract void Dispose();
    }
}
